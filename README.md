# Telegram chat parser
> The simple parser of messages and member from chat 



One to two paragraph statement about your product and what it does.

![](header.png)

## Installation

OS X & Linux:

```sh
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Windows:

```sh
python -m venv venv
./venv/Script/activate
pip install -r requirements.txt
```

## Usage example

A project can be used for parsing chat members and message in messenger Telegram

Login in https://my.telegram.org and create (API development tools)
![img.png](images/img.png)
![img_2.png](images/img_2.png)

Change
  ```python
    USER_NAME = "https://t.me/"
    PHONE = 'PHONE'
    API_HASH = "API_HASH"
    API_ID = 0
  ```
```sh
python parser.py
```

## Contributing

1. Fork it (<https://gitlab.com/litck3r/telegramchatparser/>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
