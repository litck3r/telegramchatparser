from main_dependency.modules.config_module import ConfigModule
from main_dependency.modules.local_storage import LocalStorage
from main_dependency.modules.parser_module.parser_module import ParserModule

from modules.ini_config_module.ini_config_module import IniConfigModule
from modules.input_config_module.input_config_module import InputConfigModule
from modules.local_storage import ExcelStorage
from modules.telegram_parser.telegram_parser import TelegramParserModule


class ModuleManager:
    _config_module: ConfigModule
    _parser_module: ParserModule
    _local_storage: LocalStorage

    def __init__(self):
        # self._config_module = IniConfigModule(config_path='config.ini')
        self._config_module = InputConfigModule()
        self._parser_module = TelegramParserModule(self._config_module.get_config().get_parser_config())
        self._local_storage = ExcelStorage()

    def get_parser_module(self) -> ParserModule:
        return self._parser_module

    def get_config_module(self) -> ConfigModule:
        return self._config_module

    def get_local_storage(self) -> LocalStorage:
        return self._local_storage
