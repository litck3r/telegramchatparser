import abc

from models.config_bl_model import ConfigBLModel


class ConfigModule(abc.ABC):
    @abc.abstractmethod
    def load_config(self) -> None:
        pass

    @abc.abstractmethod
    def get_config(self) -> ConfigBLModel:
        pass
