import abc
from typing import List


class LocalStorage(abc.ABC):
    @abc.abstractmethod
    def save_data(self, file_name: str, data: List[dict]):
        pass
