import abc
from typing import Union, Optional, List

from models.message_bl_model import MessageBLModel
from models.user_bl_model import UserBLModel


class ParserModule(abc.ABC):
    @abc.abstractmethod
    def get_chat_member_list(self, chat_id: Union[str, int]) -> List[UserBLModel]:
        pass

    @abc.abstractmethod
    def get_chat_message_list(self, chat_id: Union[str, int], limit: Optional[int] = None,
                              offset: Optional[int] = None) -> List[MessageBLModel]:
        pass

    @abc.abstractmethod
    def get_full_message_list(self, chat_id: Union[str, int]) -> List[MessageBLModel]:
        pass
