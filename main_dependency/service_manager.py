from main_dependency.module_manager import ModuleManager

from main_dependency.service.chat_parser import ChatParser
from service.telegram_chat_parser import TelegramChatParser


class ServiceManager:
    _chat_parser: ChatParser

    def __init__(self, module_manager: ModuleManager):
        self._chat_parser = TelegramChatParser(module_manager.get_parser_module(), module_manager.get_local_storage())

    def get_chat_parser(self) -> ChatParser:
        return self._chat_parser
