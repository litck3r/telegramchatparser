from typing import Union

from main_dependency.modules.local_storage import LocalStorage
from main_dependency.modules.parser_module.parser_module import ParserModule
from main_dependency.service.chat_parser import ChatParser


class TelegramChatParser(ChatParser):
    _local_storage: LocalStorage
    _parser_module: ParserModule

    def __init__(self,
                 parser_module: ParserModule,
                 local_storage: LocalStorage
                 ):
        self._parser_module = parser_module
        self._local_storage = local_storage

    def parse_chat(self, user_id: Union[str, int]):
        member_list = [dict(
            user_id=str(chat_member.user_id),
            first_name=chat_member.first_name,
            last_name=chat_member.last_name if chat_member.first_name else "",
            username=f"@{chat_member.username}" if chat_member.username else "",
            phone=str(chat_member.phone) if chat_member.phone else "",
        ) for chat_member in self._parser_module.get_chat_member_list(user_id)]
        full_message_list = [dict(
            message_id=str(chat_message.message_id),
            text=str(chat_message.text).replace('\n', '') if chat_message.text else "",
            user_id=str(chat_message.user_id)
        ) for chat_message in self._parser_module.get_full_message_list(user_id)]
        self._local_storage.save_data(f"{user_id}_message_list.xlsx", full_message_list)
        self._local_storage.save_data(f"{user_id}_member_list.xlsx", member_list)
