import configparser

from main_dependency.modules.config_module import ConfigModule
from models.parser_config_bl_model import ParserConfigBLModel
from models.config_bl_model import ConfigBLModel


class IniConfigModule(ConfigModule):
    config_path: str
    _config: ConfigBLModel

    def __init__(self, config_path: str):
        self._config_path = config_path
        self._config_parser = configparser.ConfigParser()
        self.load_config()

    def get_config(self) -> ConfigBLModel:
        return self._config

    def load_config(self) -> None:
        self._config_parser.read(self._config_path)
        try:
            parser_config = ParserConfigBLModel(
                api_id=int(self._config_parser['TelegramParser']['api_id']),
                api_hash=self._config_parser['TelegramParser']['api_hash'],
                phone=self._config_parser['TelegramParser']['phone']
            )
        except ValueError:
            raise Exception('Api id must be integer')
        if not parser_config.phone or parser_config.api_id or parser_config.api_hash:
            raise Exception('Config is empty')

        self._config = ConfigBLModel(
            parser_config=parser_config
        )
