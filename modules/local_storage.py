from typing import List

import pandas

from main_dependency.modules.local_storage import LocalStorage


class ExcelStorage(LocalStorage):
    def save_data(self, file_name: str, data: List[dict]):
        writer = pandas.ExcelWriter(file_name, engine='xlsxwriter')
        pandas.DataFrame(data).to_excel(writer, 'Data')
        writer.save()
