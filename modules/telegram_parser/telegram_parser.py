from typing import Union, Optional, List

from telethon import TelegramClient

from main_dependency.modules.parser_module.parser_module import ParserModule

from models.parser_config_bl_model import ParserConfigBLModel
from models.message_bl_model import MessageBLModel
from models.user_bl_model import UserBLModel


class TelegramParserModule(ParserModule):
    _phone: str
    _api_id: int
    _api_hash: str
    telegram_client: TelegramClient

    def __init__(self,
                 config: ParserConfigBLModel):

        self._api_hash = config.api_hash
        self._api_id = config.api_id
        self._phone = config.phone
        self.start()

    def start(self, ):
        self.telegram_client: TelegramClient = TelegramClient(
            self._phone,
            self._api_id,
            self._api_hash
        )
        self.telegram_client.start(phone=lambda: self._phone)

    def get_chat_member_list(self, chat_id: Union[str, int]) -> List[UserBLModel]:
        participants = self.telegram_client.get_participants(chat_id)
        return [
            UserBLModel(
                user_id=user.id,
                first_name=user.first_name,
                last_name=user.last_name,
                username=user.username,
                phone=user.phone
            ) for user in participants]

    def get_chat_message_list(self, chat_id: Union[str, int], limit: Optional[int] = None,
                              offset: Optional[int] = None) -> List[MessageBLModel]:
        return [
            MessageBLModel(user_id=message.from_id.user_id if message.from_id else None, message_id=message.id,
                         text=message.text)
            for message in self.telegram_client.iter_messages(chat_id, limit=limit, add_offset=offset)
        ]

    def get_entity(self, chat_id: Union[str, int]):
        return self.telegram_client.get_entity(chat_id)

    def get_full_message_list(self, chat_id: Union[str, int]) -> List[MessageBLModel]:
        message_list = self.get_chat_message_list(
            self.get_entity(chat_id),
            limit=1,
            offset=0
        )
        count = int(message_list[0].message_id)
        limit = 500
        full_message_list = []
        for i in range(0, count // 500):
            try:
                message_list = self.get_chat_message_list(
                    self.get_entity(chat_id),
                    limit=limit,
                    offset=limit * i
                )
                if not message_list:
                    break
                full_message_list += message_list
                print(f"{limit * i + limit} Ready !")
            except:
                break
        return full_message_list
