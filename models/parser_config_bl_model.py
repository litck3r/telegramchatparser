class ParserConfigBLModel:
    phone: str
    api_id: int
    api_hash: str

    def __init__(self,
                 api_id: int,
                 api_hash: str,
                 phone: str):
        self.phone = phone
        self.api_hash = api_hash
        self.api_id = api_id
