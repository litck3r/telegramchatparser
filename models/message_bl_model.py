from typing import Optional


class MessageBLModel:
    user_id: int
    text: Optional[str]
    message_id: int

    def __init__(self, user_id: int, message_id: int, text: Optional[str] = None):
        self.message_id = message_id
        self.text = text
        self.user_id = user_id
