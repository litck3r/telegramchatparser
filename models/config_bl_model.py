from models.parser_config_bl_model import ParserConfigBLModel


class ConfigBLModel:
    _parser_config: ParserConfigBLModel

    def __init__(self, parser_config: ParserConfigBLModel):
        self._parser_config = parser_config

    def get_parser_config(self) -> ParserConfigBLModel:
        return self._parser_config
