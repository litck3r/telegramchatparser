from typing import Optional


class UserBLModel:
    user_id: int
    username: Optional[str]
    first_name: str
    last_name: Optional[str]
    phone: Optional[str]

    def __init__(self, user_id: int, first_name: str,
                 username: Optional[str] = None, last_name: Optional[str] = None,
                 phone: Optional[str] = None):
        self.phone = phone
        self.last_name = last_name
        self.first_name = first_name
        self.username = username
        self.user_id = user_id
